﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

/* BounceBlue - Terje Rene Ekelund Nilsen
 * terje.nilsen@student.hive.no
 * 
 * Avsluttende oppgave i BGSDA50 
 * Høgskolen i Vestfold - 2012
 * 
 * 
 * All kode er enten min eller automatisk generert.
 * Annen kode vil være merket med kildehenvisning.
 * Videre kommentarer i koden vil være på engelsk.
 * 
 * 
 * */

namespace bounceBlue
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        /* 
         * Name: taptap:
         * Is a: integer
         * Keeps track of storyboard started or reset by user
         * Used to make sure we dont redo some preperations while in the midle a show
         * State is either 0 - not started or 1 - started
         * */
        private int taptap = 0;

        /* 
         * Name: BeginStb
         * Starts / resets the storyboard
         * */
        private void BeginStb()
        {
            if (taptap == 0)
            {
                stbBouncing.AutoReverse = false; // To make sure the balls stays down
                btnPause.IsEnabled = true; // Enable the pause-button
                btnStartStop.Content = "Reset"; // Change text on button
                taptap++; // inc taptap

                stbBouncing.Begin(); // Let the show begin!
            }
            else
            {
                // Reset and disable the pause-button
                btnPause.IsEnabled = false;
                btnPause.Content = "Pause";

                stbBouncing.Stop(); // Stops and resets the storyboard
                btnStartStop.Content = "Release"; 
                taptap = 0;
            }
        }

        /* 
         * Name: btnPause
         * Is a: button
         * Event: Click
         * Pauses / resumes the storyboard
         * */
        public void btnPause_Click(object sender, RoutedEventArgs e)
        {
            // pause, unpause
            if ((String)btnPause.Content == "Pause")
            {
                stbBouncing.Pause();
                btnPause.Content = "Resume";
            }
            else
            {
                stbBouncing.Resume();
                btnPause.Content = "Pause";
            }
        }

        /* 
         * Name: btnStartStop
         * Is a: button
         * Event: Click
         * Starts / resets the storyboard via BeginStb()
         * */
        private void btnStartStop_Click(object sender, RoutedEventArgs e)
        {
            BeginStb();
        }

        /* 
         * Name: btnAbout
         * Is a: button
         * Event: Click
         * Pauses the storyboard via btnPause_Click and navigates to the About page
         * */
        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            if (taptap == 1) // unless the show is on there is no need to pause
            {   // Pauses the show before navigating to About page
                stbBouncing.Pause();
                btnPause.Content = "Resume";
            }

            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }

        /* 
         * Name: rectLem
         * Is a: Rectangle
         * Event: Tap
         * Starts the storyboard via BeginStb(). The trapdoor.
         * */
        private void rectLem_Tap(object sender, GestureEventArgs e)
        {
            // double use of taptap:
            // I dont want taping on the trapdoor to reset the system, only to release the balls
            if (taptap == 0) { BeginStb(); }
        }
    }
}